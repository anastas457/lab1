package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b

    override fun substringCounter(s: String, sub: String): Int {
        return s.split(sub).count() - 1
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        return s.split(sub)
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        val res = mutableMapOf<String, Int>()
        for (i in s.split(sub)){
            if (res.contains(i)){
                res[i] = res.getValue(i).inc()
            }
            else {
                res += Pair(i, 1)
            }
        }
        return res
    }

    override fun isPalindrome(s: String): Boolean {
        if (s == "") {
            return false
        }
        val ns = StringBuilder(s)
        val reverse = ns.reverse().toString()
        return s.equals(reverse.toString(), ignoreCase = false)
    }

    override fun invert(s: String): String {
        val ns = StringBuilder(s)
        val reverse = ns.reverse().toString()
        return reverse.toString()
    }
}
